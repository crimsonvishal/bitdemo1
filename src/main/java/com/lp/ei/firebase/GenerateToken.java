package com.lp.ei.firebase;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

@SuppressWarnings("deprecation")
public class GenerateToken {
	
	public static String getAccessToken() throws IOException {

		  GoogleCredential googleCredential = GoogleCredential
		      .fromStream(new FileInputStream("C:\\Users\\ajay.rana\\AnypointStudio\\studio-workspace\\push-notifications-firebase\\src\\main\\resources\\service-account.json"))
		      .createScoped(Arrays.asList("https://www.googleapis.com/auth/firebase", "https://www.googleapis.com/auth/cloud-platform", "https://www.googleapis.com/auth/firebase.readonly"));
		  googleCredential.refreshToken();
		  return googleCredential.getAccessToken();
		}
}
